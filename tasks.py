import sys
import os
import json
import time
import globals
import methods
from apscheduler.schedulers.background import BackgroundScheduler
lib_path = os.path.abspath('./additional/BINS/')
sys.path.append(lib_path)
from main_from_FILE import run as get_BINS_data_from_FILE
from main_from_BINS import run as get_BINS_data_from_BINS


REQUESTS_INTERVAL_PERIOD = 4
speed = 0


def handle_client_exit():
    global speed
    if not globals.client_exists() and speed != 0:
        print('client exited')
        methods.emergency_stop()


def calculate_new_speed(obj):  # вычисляем скорость
    # print('calculate speed', obj['ACCELERATION Ax'], obj[
        #   'ACCELERATION Ay'], obj['ACCELERATION Az'])
    global speed
    try:
        speed += obj['ACCELERATION Ax'] / REQUESTS_INTERVAL_PERIOD
    except:
        pass


# преобразуем данные из БИНС в формат клиента
def handle_data_from_bins_output(content):
    global speed
    return_output = {}

    parsed_data = {}

    for line in content:  # parse data to parsed_data obj
        parsed_line = json.loads(line)
        for key in parsed_line:
            parsed_data[key] = parsed_line[key]

    calculate_new_speed(parsed_data)
    print('current speed', speed)

    return_output['speed'] = speed
    return return_output


def write_metrics_to_file(from_file='out_5.txt', get_data_method=get_BINS_data_from_FILE):
    print('get metrics', time.strftime("%Y-%m-%d %H:%M:%S"))
    get_data_method()  # call method to write data from bins
    with open(from_file, "r") as f:
        content = f.readlines()
        # f = open('status_files/serverdat.json', 'w')
        # json.dump(f, handle_data_from_bins_output(content))
        open('status_files/serverdat.json',
             'w').write(json.dumps(handle_data_from_bins_output(content)))

scheduler = BackgroundScheduler()

task_params = None

if '--bins' in sys.argv:
    task_params = lambda: write_metrics_to_file(
        from_file='out_3.txt', get_data_method=get_BINS_data_from_BINS)
else:
    task_params = lambda: write_metrics_to_file(
        from_file='out_5.txt', get_data_method=get_BINS_data_from_FILE)

job = scheduler.add_job(
    task_params, 'interval', seconds=REQUESTS_INTERVAL_PERIOD)

# test = lambda: write_metrics_to_file(
#    from_file='out_3.txt', get_data_method=get_BINS_data_from_FILE)
#job = scheduler.add_job(test, 'interval', seconds=REQUESTS_INTERVAL_PERIOD)

#
job = scheduler.add_job(handle_client_exit, 'interval', seconds=4)
