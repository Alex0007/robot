import serial
import time
import json

#SERIALPORT = "/dev/ttyUSB0"
SERIALPORT = "/dev/pts/8"

BAUDRATE = 9600
ser = serial.Serial(SERIALPORT, BAUDRATE)

config = json.loads(open('status_files/server.json').read())
s = 0
max_s = 0
reverse = 1  # 1 - вперед, 2 - назад


def to_hex(i):  # int to hex-string
    i = str(i)
    try:
        return '{0:x}'.format(int(i)).zfill(2).upper()
    except:
        return i

chassis = to_hex(1)  # идентификатор ходовой части
steerage = to_hex(2)  # рулевое устройство


# пишем байты в последовательный порт согласно архитектуре
def write_command(to, command, data=None):
    packet_length = 7
    if data:
        packet_length = 8
    packet = 'AE AE ' + \
        to_hex(to) + ' 00 ' + to_hex(command) + ' ' + \
        to_hex(packet_length) + ' ' + to_hex(0)
    if data:
        packet += ' ' + to_hex(data)
    print(packet)
    ser.write(bytes(bytearray.fromhex(packet)))


def start():
    global s, reverse
    write_command(chassis, reverse, 30)
    s = 30


def stop(to=0):
    global s, reverse
    write_command(chassis, reverse, s - to)
    s = to


def emergency_stop():
    write_command(chassis, 5)


def change_reverse(direction):
    global reverse
    if direction == 'back' and reverse == 1:
        reverse = 2
        write_command(chassis, 4)
    if direction == 'forward' and reverse == 2:
        reverse = 1
        write_command(chassis, 4)


def handle_direction(direction):
    global s, max_s

    if s == 0:
        change_reverse(direction)

    if direction == 'forward' and s > 0 and s < max_s:
        s += 1
        write_command(chassis, reverse, 1)
    if direction == 'forward' and s == 0:
        start()
    if direction == 'back' and s > 30:
        s -= 1
        write_command(chassis, reverse, 1)
    if direction == 'back' and s == 33:
        stop()
    if direction == 'left':
        write_command(steerage, 1)
    if direction == 'right':
        write_command(steerage, 2)


# switch: start, stop # direction: forward, back, left, right # angle #
# anglecam # turns
def handle_set_data(values):
    global s, max_s

    for key in values:
        print(key, '<-- key value ->>', values[key])
        if key == 'direction':
            handle_direction(values[key])
        if key == 'switch':
            # если остановка двигателя, сбросим обороты до 0
            if values['switch'] == 'stop':
                stop()
        if key == 'angle':
            pass
        if key == 'anglecam':
            pass
        if key == 'turns':
            max_s = int(
                int(values['turns']) / int(config['engine']['turnsmax']) * 100)
            # минимальное ненулевое число оборотов
            if max_s < 31 and max_s != 0:
                max_s = 31
            if max_s < s:  # если текущая скорость больше максимальной
                stop(max_s)
    return 'ok'
