import sys
import os
import json
import time
import globals
globals.last_get_php_time = time.time()

from flask import Flask, request, send_from_directory
from tasks import scheduler as scheduler
from methods import *

scheduler.start()
app = Flask(__name__, static_url_path='')


@app.route('/')
def route1():  # serve index.html
    return app.send_static_file('index.html')


@app.route('/serverdat.json', methods=['POST'])
def route2():  # POST запрос для данных с датчиков робота
    print('post /serverdat.json')
    return send_from_directory('./status_files', 'serverdat.json')


@app.route('/server.json', methods=['POST'])
def route3():  # POST запрос для получения настроек робота
    print('post /server.json')
    return send_from_directory('./status_files', 'server.json')


@app.route('/get.php', methods=['POST'])
def route4():  # POST запрос для установки значений обротов итд
    globals.last_get_php_time = time.time()
    print('post /get.php at time', globals.last_get_php_time)
    return handle_set_data(request.values)


app.run()
