###Install and run
`cd robot`

`pip install virtualenv`

`virtualenv -p python3 venv`

`source venv/bin/activate`

`pip install -r requirements.txt`

`python server.py` to work with file

`python server.py --bins` 

####Emulate serial ports
`socat -d -d pty,raw,echo=0 pty,raw,echo=0`

You can listen to port with cat

####Actions
Start / stop engine on html page

`$('#switch').click()`
