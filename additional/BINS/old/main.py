#!/usr/bin/python
# -*- coding: utf-8 -*-
import struct
import serial
import sys
import json

import id70
import id72


BYTE = 2
f = open("out.txt","w")

def fillzero(somestr):
    return somestr.zfill(4)


def toBin(binstring): #hex to bin
    finalstring = ""

    for ch in binstring:
                hexstring = bin(int(ch,16))
                hexstring = hexstring.replace("0b","",1)
                if (len(hexstring) < 4):
                    hexstring = fillzero(hexstring)
                finalstring += hexstring

    return finalstring

def parse70(data):
    #f.write(json.dumps('id_msg':'70'))
    print("by 1 bit:  " + data[:BYTE*4]+ " " + str(len(data[:BYTE*4])))
    by1string = data[:BYTE*4]

    list1 = list(toBin(by1string))[:16]
    list2 = id70.by1
    print(len(list1))
    print(len(list2))
    result = zip(list2, list1)

    for para in result:
        print(para)
	f.write(json.dumps({para[0]:para[1]}))

    s = data[BYTE*4:]
    print("by 4 byte: " + s + " " + str(len(s)))

    list3 = []
    for i in range(0, len(s), 4 * BYTE):
        list3.append(struct.unpack('!f', s[i:i + 4 * BYTE].decode('hex'))[0])
    list4 = id70.by4
    print(len(list3))
    print(len(list4))
    result = zip(list4,list3)
    for para in result:
        print(para)
	f.write(json.dumps({para[0]:para[1]}))





def parse72(data):
    #f.write(json.dumps('id_msg':'72'))
    print("by 4 byte: " + data + " " + str(len(data)))

    list1 = []
    for i in range(0, len(data), 4 * BYTE):
        list1.append(struct.unpack('!f', data[i:i + 4 * BYTE].decode('hex'))[0])

    list2 = id72.by4
    print(len(list1))
    print(len(list2))
    result = zip(list2, list1)
    for para in result:
        print(para)
	f.write(json.dumps({para[0]:para[1]}))

def mainParse(data):
    msg_id = data[0:BYTE]
    print("PARSING > > >" + msg_id)
    if (msg_id == "70"): parse70(data[BYTE:len(data)-4])
    if (msg_id == "72"): parse72(data[BYTE:len(data)-4])

ser = open("new_data.txt","r")

try:
    while 1:
        c = ser.read(2*BYTE)#.encode('hex')  #SYNCH
        length = int(ser.read(1*BYTE),16)#.encode('hex'), 16)  # LENGTH
        iddatacrc = ser.read(length*BYTE)#.encode('hex')  # ID DATA CRC
        print(c + " " + str(length) + " iddatacrc: " + iddatacrc[:BYTE])
        if (iddatacrc[0:BYTE] != "4c"):
            mainParse(iddatacrc)
except:
    ser.close()
    print(sys.exc_info()[0])




#ser = serial.Serial(
#    port='/dev/ttyUSB0',
#    baudrate=460800,
#    parity=serial.PARITY_NONE,
#    stopbits=serial.STOPBITS_ONE,
#    bytesize=serial.EIGHTBITS
#)

#if ser.isOpen():
#    thestring = "AAAA08407014000000C0A6"
#    data = thestring.decode('hex')
#    ser.write(data)
#    thestring = "AAAA0840721400000043E2"
#    data = thestring.decode('hex')
#    ser.write(data)
#    print("WRITTEN")
#try:
#    while 1:
#        c = ser.read(2).encode('hex')  #SYNCH
#        length = int(ser.read(1).encode('hex'), 16)  # LENGTH
#        iddatacrc = ser.read(length).encode('hex')  # ID DATA CRC
#        print(c + " " + str(length) + " iddatacrc: " + iddatacrc[:BYTE])
#        if (iddatacrc[0:BYTE] != "4c"):
#            mainParse(iddatacrc)
#except:
#    ser.close()
#    print(sys.exc_info()[0])



