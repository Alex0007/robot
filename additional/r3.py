import serial
import time
import Tkinter

SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 9600
ser = serial.Serial(SERIALPORT, BAUDRATE)
s = 0

def start():
    global s
    for i in range(31):
        ser.write("+")
        time.sleep(0.01)
        s += 1

def stop():
    global s
    for i in range(31):
        ser.write("-")
        time.sleep(0.01)
        s -= 1

def f(e,tk):
    global s
    print("s=",s)
    c = e.char
    if c == '`': exit()
    if c == 'w' and s > 0 :
        ser.write("+")
        s += 1
    if c == 'w' and s == 0 :
        start()
    if c == 's' and s > 30:
        ser.write("-")
        s -= 1
    if c == 's' and s == 33:
        stop()
    if c == 'a':
        ser.write("3")
    if c == 'd':
        ser.write("4")
    if c == ' ':
        ser.write("0")
        s = 0
    if c == 'c':
        ser.write("0")
        ser.write("1")
        s = 0

if ser.isOpen():

    try:
        ser.flushInput() #flush input buffer, discarding all its contents
        ser.flushOutput()#flush output buffer, aborting current output

        tk = Tkinter.Tk()
        tk.bind_all('<Key>',  lambda event: f(event, tk))
        tk.mainloop()

    except Exception, e:
        print "error communicating...: " + str(e)

else:
    print "cannot open serial port "

